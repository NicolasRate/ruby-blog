class Article < ApplicationRecord
  include Visible

  has_one_attached :image, dependent: :destroy
  has_many :comments, dependent: :destroy
  belongs_to :author, class_name: 'User'

  validates :title, presence: true
  validates :body, presence: true, length: { minimum: 10 }

  scope :ordered_by_desc, -> (direction = :desc) { order(created_at: direction)}
  scope :ordered_by_asc, -> (direction = :asc) { order(created_at: direction)}
  scope :ordered_by_title, -> (direction = :asc) { order(title: direction)}
  scope :with_authors, -> {includes(:author)}
end
