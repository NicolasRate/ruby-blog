class ArticlesController < ApplicationController
    before_action :authenticate_user!, only: %i[new create edit update destroy]
    before_action :set_article, only: %i[show edit update destroy]

    # http_basic_authenticate_with name: "dhh", password: "secret", except: [:index, :show]
    # def index
    #   if param[:sort] == "Title"
    #     @articles = Article.ordered.with_authors.paginate(page: params[:page], per_page: 2).where('title LIKE ?', "#{params[:search]}%")
    #   @articles = Article.ordered.with_authors.paginate(page: params[:page], per_page: 2).where('title LIKE ?', "#{params[:search]}%")
    # end
  def index
    @sort_type = params.fetch(:sort_type, "latest")
    @articles = Article.with_authors.paginate(page: params[:page], per_page: 3).where('title LIKE ?', "#{params[:search]}%")
    if @sort_type == "title"
      @articles = @articles.ordered_by_title
    elsif @sort_type == "oldest"
      @articles = @articles.ordered_by_asc
    else 
      @articles = @articles.ordered_by_desc
    end
  end

  def show
    @article = Article.find(params[:id])
  end

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(article_params)

    @article.author = current_user

    unless article_params[:image].nil?
      image = Cloudinary::Uploader.upload(article_params[:image], options = {})
    end

    if @article.save
      redirect_to @article
    else
      render :new
    end
  end

  def edit
    authorize @article = Article.find(params[:id])
  end

  def update
    authorize @article = Article.find(params[:id])

    if @article.update(article_params)
      redirect_to @article
    else
      render :edit
    end
  end

  def destroy
    authorize @article = Article.find(params[:id])
    @article.destroy

    redirect_to root_path
  end

  private
  def article_params
    params.require(:article).permit(:title, :body, :status, :image)
  end

  def set_article
    @article = Article.find(params[:id])
  end

  def authorize_user!
    return if @article.author_id == current_user.id

    redirect_to :articles,
                alert: "You are not allowed to perform this acrion"
  end

end
